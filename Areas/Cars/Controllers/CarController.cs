using System.Linq;
using blueSky.Areas.Cars.Models;
using blueSky.Controllers;
using blueSky.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace blueSky.Areas.Cars.Controllers
{
    public class CarsController : HomeController
    {
        private readonly BlueSkyContext _context;


        public CarsController(BlueSkyContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index(CarIndexViewModel viewModel)
        {
            var car = _context.Cars.Include(x => x.Maintenance);

            viewModel.GetCars = car.ToList();
         
            return View(viewModel);
        }


        [HttpPost]
        public IActionResult Add(Car car)
        {
            _context.Cars.Add(car);
            _context.SaveChanges();
            
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            // ReSharper disable once Mvc.ViewNotResolved
            return View();
        }

        public IActionResult Delete()
        {
            return RedirectToAction("Index");

        }
    }
}