using System;
using System.Collections.Generic;
using blueSky.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace blueSky.Areas.Cars.Models
{
    public class CarIndexViewModel
    {
        public Car Car { get; set;}
        public List<Car> GetCars { get; set; }
    }
}