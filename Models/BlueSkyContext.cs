using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace blueSky.Models
{
    public class BlueSkyContext : DbContext
    {
        public BlueSkyContext (DbContextOptions<BlueSkyContext> options): base(options)
        {
        }

        public DbSet<blueSky.Models.Car> Cars { get; set; }
        public DbSet<blueSky.Models.Maintenance> Maintenances { get; set; }
    }
}