using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace blueSky.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Nickname { get; set; }

        public List<Maintenance> Maintenance {get; set;}
    }
}