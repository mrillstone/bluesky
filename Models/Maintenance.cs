using System;
using System.ComponentModel.DataAnnotations;


namespace blueSky.Models
{
    public class Maintenance
    {
        public int Id { get; set; }

        public int CarId { get; set; }
        public DateTime Date { get; set;}
        public String Title {get; set;}
        public String Description {get; set;}
        public int Miles {get; set;}
        public String Product {get; set;}
    }
}